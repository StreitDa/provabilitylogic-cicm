(*<*)
theory CICMPaper

imports Main
begin
(*>*)
section {*Introduction*}


text {*In the second half of the twentieth century, building on work from Kurt Gödel and Martin Löb,
it became apparent that the underlying logic of statements about provability in formal arithmetical
systems is a \emph{modal} logic. Traditionally, modal logics have been the focus of philosophical
rather than mathematical interest as they are usually taken to express notions of necessity and
possibility. Perhaps surprisingly, they can also express formally the notions of provability in arithmetic.
Today this logic has been studied extensively and is known as GL, named after Gödel and Löb.*}

text{*We present a shallow semantic embedding for GL in the typed higher-order logic HOL, that has been 
shown to perform quite well in
initial experiments. The embedding uses the proof assistant Isabelle/HOL \cite{Isabelle} and allows the user to reason
about GL in Isabelle interactively and provides some support for proof automation.*}

text {*The entirety of this document is typeset directly from Isabelle. It is thus both a description
and a demonstration of the implementation of GL. All proofs are verified automatically against the
embedding. To show the efficacy of the embedding we demonstrate that we can prove some theorems
in George Boolos' standard text on Provability Logic: \emph{The Logic of Provability} \cite{Boolos1995}.

We also show that, using this embedding, Isabelle is able to automatically prove Gödel's second 
Incompleteness Theorem as represented in GL. The embedding can thus be used as a modular part for 
reasoning about provability built on top of future implementations of formal systems
strong enough for the Incompleteness Theorem to hold.*}

text {*GL is a propositional normal modal logic that adds
the axiom scheme $\Box (\Box \phi \rightarrow \phi) \rightarrow \Box \phi$ to the simplest normal 
modal logic K.

Its semantics can be given via Kripke frames with a transitive and converse well-founded accessibility
relation. \cite{Boolos1995}*}

section{*Implementation*}

text {* The implementation we use is based on earlier work by Benzmüller and Paulson \cite{BenzmuellerPaulson2013},
which proves the faithfulness of embedding normal modal logics in HOL in such a way.*}

text{*The embedding uses the correspondence between transitive, converse well-founded Kripke frames
and provable statements in GL. As with Kripke frames we introduce a type for possible worlds*}
typedecl i (*-- "type for possible worlds" *)
text{* and a type for the formulas in the embedding of type @{typ "i\<Rightarrow>bool"}, which we will abbreviate
as follows.*}
type_synonym \<sigma> = "(i\<Rightarrow>bool)"

text{*We model the accessibility relation as a constant of type @{typ "i\<Rightarrow>i\<Rightarrow>bool"}.*}

consts r :: "i\<Rightarrow>i\<Rightarrow>bool" (infixr "r" 70)


text {* A formula in the embedding can thus be understood as something that is true in a possible world. With this in
mind, the connectives can be defined using lambda terms in the expected way.
The box operator returns a formula that is true
in a world iff it is true in all accessible worlds, the diamond operator is defined as the dual of the
box operator for convenience. To differentiate the modal connectives from regular HOL connectives, 
they are typeset in bold. Isabelle internally translates occurrences of these connectives to their
definition, allowing a high degree of automation using internal as well as external provers 
 such as Vampire \cite{Vampire} and Leo-II/Leo-III \cite{Leo-II, Leo-III}.*}
abbreviation mtrue  :: "\<sigma>" ("\<^bold>\<top>")
  where "\<^bold>\<top> \<equiv> \<lambda>w. True" 
(*<*)abbreviation mfalse :: "\<sigma>" ("\<^bold>\<bottom>") 
  where "\<^bold>\<bottom> \<equiv> \<lambda>w. False"
(*>*)

abbreviation mnot   :: "\<sigma>\<Rightarrow>\<sigma>" ("\<^bold>\<not>_"[52]53)
  where "\<^bold>\<not>\<phi> \<equiv> \<lambda>w. \<not>\<phi>(w)" 
abbreviation mand   :: "\<sigma>\<Rightarrow>\<sigma>\<Rightarrow>\<sigma>" (infixr"\<^bold>\<and>"51)
  where "\<phi>\<^bold>\<and>\<psi> \<equiv> \<lambda>w. \<phi>(w)\<and>\<psi>(w)"
abbreviation mor    :: "\<sigma>\<Rightarrow>\<sigma>\<Rightarrow>\<sigma>" (infixr"\<^bold>\<or>"50)
  where "\<phi>\<^bold>\<or>\<psi> \<equiv> \<lambda>w. \<phi>(w)\<or>\<psi>(w)"   
abbreviation mimp   :: "\<sigma>\<Rightarrow>\<sigma>\<Rightarrow>\<sigma>" (infixr"\<^bold>\<rightarrow>"49) 
  where "\<phi>\<^bold>\<rightarrow>\<psi> \<equiv> \<lambda>w. \<phi>(w)\<longrightarrow>\<psi>(w)"
abbreviation mequ   :: "\<sigma>\<Rightarrow>\<sigma>\<Rightarrow>\<sigma>" (infixr"\<^bold>\<leftrightarrow>"48)
  where "\<phi>\<^bold>\<leftrightarrow>\<psi> \<equiv> \<lambda>w. \<phi>(w)\<longleftrightarrow>\<psi>(w)"  
abbreviation mbox   :: "\<sigma>\<Rightarrow>\<sigma>" ("\<^bold>\<box>_"[52]53)
  where "\<^bold>\<box>\<phi> \<equiv> \<lambda>w.\<forall>v. w r v \<longrightarrow> \<phi>(v)"
abbreviation mdia :: "\<sigma>\<Rightarrow>\<sigma>" ("\<^bold>\<diamond>_"[52]53)
  where  "\<^bold>\<diamond>\<phi> \<equiv> \<^bold>\<not>\<^bold>\<box>(\<^bold>\<not>\<phi>)"

text {* Finally, a formula is valid if and only if it is true in all worlds. For 
enhanced readability, we use brackets around formulas for validity.*}

abbreviation valid :: "\<sigma>\<Rightarrow>bool" ("\<lfloor>_\<rfloor>"[8]109)
  where "\<lfloor>p\<rfloor> \<equiv> \<forall>w. p w"

text {*Sometimes it is useful to be able to talk about concepts such as "true in a world", 
or valid, given that the axiomatization of a certain logic holds. We can model these in the embedding
like this:*}
abbreviation follows_w :: "i \<Rightarrow> \<sigma> \<Rightarrow> bool" (infix"\<^bold>\<Turnstile>"55)
  where "(w \<^bold>\<Turnstile> p) \<equiv> p w"
abbreviation follows_glob :: "bool \<Rightarrow> \<sigma> \<Rightarrow> bool" (infix"\<^bold>\<turnstile>"40)
  where "(L \<^bold>\<turnstile> p) \<equiv> L \<longrightarrow> \<lfloor>p\<rfloor>"

text {*We can then implement different normal modal logics via their frame conditions. \cite{ModalCube}
For now we include only transitivity and converse well-foundedness. Other normal modal logics have
been implemented by Benzmüller et al.. \cite{ModalCube}

Converse well-foundedness is usually defined via sets. A relation R on a set is converse well-founded
iff for each nonempty subset there exists an element x in the subset,
such that there is no element y in the subset such that \mbox{$x R y$}. For computational efficacy we use
the characteristic function of type \mbox{@{typ"i \<Rightarrow> bool"}} of a set instead of using the set itself.

Another \mbox{-- more intuitive but in this case computationally more demanding --}
 way to define converse well-foundedness 
is the property of there being no infinite ascending chains like
\mbox{$x_1 R x_2 R ...$}.*}

(*<*)
(*Common accessibility relations*)
abbreviation reflexive 
  where "reflexive \<equiv> (\<forall>x. x r x)"
abbreviation symmetric 
  where "symmetric \<equiv> (\<forall>x y. x r y \<longrightarrow> y r x)"
abbreviation serial :: "bool"
  where "serial \<equiv> (\<forall>x. \<exists>y. x r y)"
(*>*)
abbreviation transitive :: "bool"
  where "transitive \<equiv> (\<forall>x y z. ((x r y) \<and> (y r z) \<longrightarrow> (x r z)))"
(*<*)
abbreviation euclidean :: "bool"
  where "euclidean \<equiv> (\<forall>x y z. ((x r y) \<and> (x r z) \<longrightarrow> (y r z)))"
abbreviation irreflexive :: "bool"
  where  "irreflexive \<equiv>\<forall>x. \<not>(x r x)"
(*>*)

abbreviation converseWellFounded :: "bool"
  where "converseWellFounded  \<equiv> \<forall>S::i\<Rightarrow>bool. (\<exists>a.(S(a))) \<longrightarrow> (\<exists>m.(S(m) \<and> (\<forall>s.(S(s) \<longrightarrow> (\<not> (m r s))))))" 

text {* Using these definitions, we can derive an axiomatization for GL using the semantic embedding.*}
(*<*)
(*Common embeddings for (other) normal modal logics.*)
abbreviation D :: bool 
 where "D  \<equiv> serial"
abbreviation B :: bool 
 where "B  \<equiv> symmetric"
abbreviation T :: bool
 where "T  \<equiv> reflexive"
abbreviation K4 :: bool
 where "K4  \<equiv> transitive"
abbreviation S4 :: bool
 where "S4  \<equiv> reflexive \<and> transitive"
abbreviation S5 :: bool 
 where "S5  \<equiv> reflexive \<and> euclidean"
abbreviation GL :: bool
(*>*)
  where "GL  \<equiv> converseWellFounded \<and> transitive"

section {*Experiments*}
text {*Finally, we show that the embedding can be fruitfully employed to use \mbox{Isabelle} as an interactive
theorem prover for GL with the possibility to automate some parts entirely. We demonstrate this by showing
that we can reason about theorems of GL as well as some meta-language statements. These theorems are
taken from Boolos \cite{Boolos1995}.*}
(*<*)

(*Define Boxdot operator*)
abbreviation boxand  ("\<boxdot> _"[52]53)
  where  "\<boxdot> p::\<sigma>  \<equiv> (\<^bold>\<box>p \<^bold>\<and> p)"


(*CHAPTER 1 OF BOOLOS*)

lemma Chap1Theorem1: "(L \<^bold>\<turnstile> (p \<^bold>\<rightarrow> q)) \<longrightarrow> (L \<^bold>\<turnstile> (\<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<box>q))" by blast

lemma Chap1Theorem2: "(L \<^bold>\<turnstile> (p \<^bold>\<leftrightarrow> q)) \<longrightarrow> (L \<^bold>\<turnstile> (\<^bold>\<box>p \<^bold>\<leftrightarrow> \<^bold>\<box>q))" by blast

lemma Chap1Theorem3: "(L \<^bold>\<turnstile> \<^bold>\<box>(p \<^bold>\<and> q)) \<longrightarrow> (L \<^bold>\<turnstile> (\<^bold>\<box>p \<^bold>\<and> \<^bold>\<box>q))" by blast


(*Define conjunction of a list of formulas; do the same modalized*)
primrec conjlist :: "\<sigma> list \<Rightarrow> \<sigma>"  where 
      "conjlist [] = \<^bold>\<top>" (*Empty Conjunction evaluates as True*)
      | "conjlist (x#xs) = x \<^bold>\<and> conjlist xs"
primrec conjboxlist :: "\<sigma> list \<Rightarrow> \<sigma>"  where 
      "conjboxlist [] = \<^bold>\<box>\<^bold>\<top>" (*Empty Conjunction evaluates as nec True*)
      | "conjboxlist (x#xs) = \<^bold>\<box>x \<^bold>\<and> conjboxlist xs"

text {* With these definitions we can capture theorems that include arbitrary conjunctions.*}

lemma Chap1Theorem4: "\<forall>S::\<sigma> list. (L \<^bold>\<turnstile> \<^bold>\<box>(conjlist S) \<^bold>\<leftrightarrow> conjboxlist(S))"
  proof - 
    {fix S
    have "L  \<^bold>\<turnstile>(\<^bold>\<box>(conjlist S)) \<^bold>\<leftrightarrow> conjboxlist(S)"
      proof (induct S)
        case Nil
        then show ?case by simp
      next
        case (Cons a S)
        then show ?case by (metis conjboxlist.simps(2) conjlist.simps(2))
      qed}
    from this show ?thesis by blast
   qed

lemma Chap1theorem5: "\<forall>S::\<sigma> list. L \<^bold>\<turnstile> conjlist(S) \<^bold>\<rightarrow> C \<longrightarrow> L \<^bold>\<turnstile> conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C"
  proof -
    {fix S
    {assume "L \<^bold>\<turnstile> conjlist(S) \<^bold>\<rightarrow> C "
    have "L \<^bold>\<turnstile> conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C"
      using \<open>L \<^bold>\<turnstile> (\<lambda>w. w \<^bold>\<Turnstile> (conjlist S \<^bold>\<rightarrow> C))\<close> Chap1Theorem4 by fastforce}
  hence "L \<^bold>\<turnstile> conjlist(S) \<^bold>\<rightarrow> C \<longrightarrow> L \<^bold>\<turnstile> conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C" by blast}
  from this show ?thesis by blast
qed

lemma Chap1Theorem6: "(L \<^bold>\<turnstile> (p \<^bold>\<rightarrow> q)) \<longrightarrow> (L \<^bold>\<turnstile> (\<^bold>\<diamond>p \<^bold>\<rightarrow> \<^bold>\<diamond>q))" by blast

lemma Chap1Theorem7: "(L \<^bold>\<turnstile> (p \<^bold>\<leftrightarrow> q)) \<longrightarrow> (L \<^bold>\<turnstile> (\<^bold>\<diamond>p \<^bold>\<leftrightarrow> \<^bold>\<diamond>q))" by blast

lemma Chap1Theorem8: "(L \<^bold>\<turnstile> \<^bold>\<diamond>p \<^bold>\<and> \<^bold>\<box>q) \<longrightarrow> (L \<^bold>\<turnstile> \<^bold>\<diamond> (p \<^bold>\<and> q))" by blast

lemma Chap1Theorem9a: "K4 \<^bold>\<turnstile> ((\<boxdot> p) \<^bold>\<leftrightarrow> (\<boxdot> \<boxdot> p))" by blast
lemma Chap1Theorem9b: "K4 \<^bold>\<turnstile> ((\<boxdot>(\<^bold>\<box>p)) \<^bold>\<leftrightarrow> \<^bold>\<box>p)" by blast 
lemma Chap1Theorem9c: "K4 \<^bold>\<turnstile> ((\<boxdot>(\<^bold>\<box>p)) \<^bold>\<leftrightarrow> (\<^bold>\<box>(\<boxdot> p)))" by blast

(*Abbrevs for extension/inclusion of logics*)
abbreviation is_extended_by (infix "\<^bold>\<subseteq>"30)
  where "L1 \<^bold>\<subseteq> L2 \<equiv> \<forall>p.((L1 \<^bold>\<turnstile> p) \<longrightarrow> (L2 \<^bold>\<turnstile> p))"
abbreviation extends (infix "\<^bold>\<supseteq>"30)
  where "L1 \<^bold>\<supseteq> L2 \<equiv> L2 \<^bold>\<subseteq> L1"

lemma Chap1Theorem10: "(L \<^bold>\<supseteq> K4 \<and> (L \<^bold>\<turnstile> (\<boxdot> p) \<^bold>\<rightarrow> q)) \<longrightarrow> (L \<^bold>\<turnstile> \<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<box>q)" by metis

lemma Chap1Theorem11: "K4 \<^bold>\<turnstile> (\<^bold>\<box>\<^bold>\<diamond>\<^bold>\<box>\<^bold>\<diamond>p \<^bold>\<leftrightarrow>(\<^bold>\<box>\<^bold>\<diamond>p))"
proof -
  have a: "K4 \<^bold>\<turnstile> (\<^bold>\<box>\<^bold>\<diamond>\<^bold>\<box>\<^bold>\<diamond>p \<^bold>\<rightarrow> (\<^bold>\<box>\<^bold>\<diamond>p))" by meson
  have b: "K4 \<^bold>\<turnstile> (\<^bold>\<box>\<^bold>\<diamond>p) \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>\<^bold>\<box>\<^bold>\<diamond>p" by blast
  from a b show ?thesis by blast
qed

lemma Chap1Theorem12a: "T \<^bold>\<turnstile> (p \<^bold>\<rightarrow> \<^bold>\<diamond>p)" by blast
lemma Chap1Theorem12b: "T \<^bold>\<turnstile> (\<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<diamond>p)" by blast


lemma Chap1Theorem13: "S4 \<^bold>\<turnstile> (\<^bold>\<diamond>\<^bold>\<diamond>p \<^bold>\<rightarrow> \<^bold>\<diamond>p)" by blast

lemma Chap1Theorem14a: "S4 \<^bold>\<turnstile> (\<^bold>\<box>p \<^bold>\<leftrightarrow> \<^bold>\<box>\<^bold>\<box>p)" by blast
lemma Chap1Theorem14b: "S4 \<^bold>\<turnstile> (\<^bold>\<diamond>p \<^bold>\<leftrightarrow> \<^bold>\<diamond>\<^bold>\<diamond>p)" by blast

(*Theorem 15 is difficult to represent in 2 dimensions; Theorem 16 cannot easily be captured using
this embedding*)

lemma Chap1Theorem17: "S5 \<^bold>\<turnstile> (\<^bold>\<diamond>p \<^bold>\<leftrightarrow> \<^bold>\<diamond>\<^bold>\<diamond>p) \<^bold>\<and> (\<^bold>\<box>p \<^bold>\<leftrightarrow> \<^bold>\<box>\<^bold>\<box>p) \<^bold>\<and> (\<^bold>\<box>\<^bold>\<diamond>p \<^bold>\<leftrightarrow> \<^bold>\<diamond>p) \<^bold>\<and> (\<^bold>\<diamond>\<^bold>\<box>p \<^bold>\<leftrightarrow> \<^bold>\<box>p)" by meson

(*>*)

text {*Some elementary proofs can be fully automated using Isabelle's built-in proof routines like meson and metis.*}
theorem Chap1Theorem18: "GL \<^bold>\<turnstile> \<^bold>\<box>A \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<box>A" by meson
theorem Chap1Theorem21: "GL \<^bold>\<turnstile> \<^bold>\<box>\<^bold>\<bottom> \<^bold>\<leftrightarrow> \<^bold>\<box>\<^bold>\<diamond>A" by metis
theorem Chap1Theorem24a: "GL \<^bold>\<turnstile> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>p) \<^bold>\<leftrightarrow> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<bottom>)" by metis

text {*Other theorems need some human input, but can be proven interactively just like 
one would prove such statements with pen and paper (see Figure \ref{Fig}). In our experiments, approximately
half of all such elementary theorems in the first chapter of Boolos could be automated, the others, 
like the following,
required at least some human input. All theorems of chapter one and four that can be stated using this embedding have been proven.
They can be found online together with the source code of this paper. \cite{SOURCE}*}
(*<*)
theorem Chap1Theorem19a1: "GL \<longrightarrow> \<lfloor>\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A) \<^bold>\<rightarrow> \<^bold>\<box>A\<rfloor>"
proof - (*Example proof of how interactive proof development can work using the embedding*)
  {assume GL
  hence Conv: "converseWellFounded" by blast
  have "\<lfloor>\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A) \<^bold>\<rightarrow> \<^bold>\<box>A\<rfloor>"
  proof -    
{fix p (*choose a proposition*)
  {fix w::i (* choose a world  *)
    {assume b: "\<not>(w \<^bold>\<Turnstile>(\<^bold>\<box> p))"
      have c: "\<exists>z::i.(w r z) \<and> \<not> p(z)" using b by blast
      let ?set_X = "\<lambda>x.(w r x)  \<and> (\<not> x \<^bold>\<Turnstile> p)"
      have knark: "\<exists>x. ?set_X(x)" using b by blast
      from Conv have d:"(\<exists>a.(?set_X(a))) \<longrightarrow> (\<exists>m.(?set_X(m) \<and> (\<forall>s.(?set_X(s) \<longrightarrow> (\<not>(m r s))))))" 
        by (rule allE)
      hence "\<exists>x.(?set_X(x) \<and> (\<forall>s.(?set_X(s) \<longrightarrow> (\<not> x r s))))" using knark by blast
      from this obtain x where obtx: "?set_X(x) \<and> (\<forall>s.(?set_X(s) \<longrightarrow> (\<not> x r s)))" by blast
      fix y
      have e: "w r x" by (simp add: obtx)
      {assume asm: "x r y"
        hence "w r y" using obtx by (meson \<open>GL\<close>)
        hence g: "y \<^bold>\<Turnstile> p" using obtx asm by blast
        hence "\<not> ?set_X (y)" by simp
        hence "x \<^bold>\<Turnstile> (\<^bold>\<box>p)" by (meson obtx \<open>GL\<close>) 
        hence "\<not>(x \<^bold>\<Turnstile> (\<^bold>\<box>p \<^bold>\<rightarrow> p))" by (simp add: obtx)
        hence "\<not>(w \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p)))" using \<open>x \<^bold>\<Turnstile> (r) w\<close> by blast
        hence "w \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)" by blast}
      {assume "\<not>(x r y)" 
        have "w \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)" by (metis obtx \<open>GL\<close>) }
      hence "w \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)" using obtx by (metis \<open>GL\<close>)}
    hence f: "\<not>(w \<^bold>\<Turnstile>(\<^bold>\<box> p)) \<longrightarrow> (w \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p))" by blast
    {assume "(w \<^bold>\<Turnstile>(\<^bold>\<box> p))"
      hence "w \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)" by auto}
    hence "w \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)" using \<open>GL\<close> f by blast}
  hence "\<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>" by blast}
  thus ?thesis by blast
  qed
 hence "\<lfloor>\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A) \<^bold>\<rightarrow> \<^bold>\<box>A\<rfloor>" by blast} 
from this show ?thesis by blast
qed

theorem Chap1Theorem19a2: "GL \<^bold>\<turnstile> \<^bold>\<box>A \<^bold>\<rightarrow>  \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A)" by metis
(*>*)
theorem Chap1Theorem19a: "GL \<^bold>\<turnstile> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A) \<^bold>\<leftrightarrow> \<^bold>\<box>A"
(*<*)
proof - (*Only smt proofs are found with sledgehammer; blast fails to prove directly.*)
    from Chap1Theorem19a1 have l: "GL \<^bold>\<turnstile> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A) \<^bold>\<rightarrow> \<^bold>\<box>A" by -
    from Chap1Theorem19a2 have  "GL \<^bold>\<turnstile> \<^bold>\<box>A \<^bold>\<rightarrow>  \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A)" by -
    thus "GL \<^bold>\<turnstile> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A) \<^bold>\<leftrightarrow> \<^bold>\<box>A" using l by blast
qed

theorem Chap1Theorem19b1: "GL \<^bold>\<turnstile> \<^bold>\<box>A \<^bold>\<rightarrow> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A)" by meson
theorem Chap1Theorem19b2: "GL \<^bold>\<turnstile> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A) \<^bold>\<rightarrow> \<^bold>\<box>A" by meson
(*>*)

theorem Chap1Theorem19b: "GL \<^bold>\<turnstile> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A) \<^bold>\<leftrightarrow> \<^bold>\<box>A"
(*<*)   using Chap1Theorem19b1 Chap1Theorem19b2 by metis


(*Theorem 20 slightly reformulated bc. of definitions already in use but equivalent*)
theorem Chap1Theorem20: "\<forall>S::\<sigma> list. (GL \<^bold>\<turnstile> (conjboxlist(S) \<^bold>\<and> conjlist(S) \<^bold>\<and> \<^bold>\<box>C) \<^bold>\<rightarrow> C) \<longrightarrow> (GL \<^bold>\<turnstile> conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C)" 
proof - 
  {fix S
  {assume asm1: "GL \<^bold>\<turnstile> (conjboxlist(S) \<^bold>\<and> conjlist(S) \<^bold>\<and> \<^bold>\<box>C) \<^bold>\<rightarrow> C"
    hence a:"GL \<^bold>\<turnstile> \<^bold>\<box>(conjboxlist(S) \<^bold>\<and> conjlist(S)) \<^bold>\<rightarrow>\<^bold>\<box>(\<^bold>\<box>C \<^bold>\<rightarrow> C)" using Chap1Theorem1 by fast
    have "(GL \<^bold>\<turnstile> conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C)" 
      proof -
        {assume Lo: GL
          {fix w::i
          have "(\<^bold>\<box>(conjboxlist(S) \<^bold>\<and> conjlist(S)) \<^bold>\<leftrightarrow> conjboxlist(S)) w" using Chap1Theorem4 by (metis Lo)
          hence b: "(conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>(\<^bold>\<box>C \<^bold>\<rightarrow> C)) w"  using Lo a by blast
          from Chap1Theorem19a have "\<lfloor>(\<^bold>\<box>(\<^bold>\<box>C \<^bold>\<rightarrow> C) \<^bold>\<leftrightarrow> \<^bold>\<box>C)\<rfloor>" using Lo by (rule mp)
          hence "(\<^bold>\<box>(\<^bold>\<box>C \<^bold>\<rightarrow> C) \<^bold>\<leftrightarrow> \<^bold>\<box>C) w" by presburger
          hence "(conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C) w" using b by argo}
        hence "\<lfloor>(conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C)\<rfloor>" by blast}
    thus ?thesis by blast qed}
  hence "(GL  \<^bold>\<turnstile> (conjboxlist(S) \<^bold>\<and> conjlist(S) \<^bold>\<and> \<^bold>\<box>C) \<^bold>\<rightarrow> C) \<longrightarrow> (GL \<^bold>\<turnstile> conjboxlist(S) \<^bold>\<rightarrow> \<^bold>\<box>C)" by blast}
  thus ?thesis by blast
qed

theorem Chap1Theorem22: "GL \<^bold>\<turnstile> \<^bold>\<box>\<^bold>\<diamond>\<^bold>\<bottom> \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<bottom>" using Chap1Theorem21 by meson


lemma Chap1Theorem23: "K4 \<^bold>\<turnstile> \<^bold>\<box>(q \<^bold>\<leftrightarrow> (\<^bold>\<box>q \<^bold>\<rightarrow> p)) \<^bold>\<rightarrow> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)" 
proof - 
  {assume Lo: K4
    {fix w::i
      {assume l: "(\<^bold>\<box>(q \<^bold>\<leftrightarrow> (\<^bold>\<box>q \<^bold>\<rightarrow> p))) w"
        hence a: "(\<^bold>\<box> (\<^bold>\<box>q \<^bold>\<rightarrow> \<^bold>\<box>p)) w" using Lo  by blast
        from l Lo have "(\<^bold>\<box>q \<^bold>\<rightarrow> \<^bold>\<box>p) w" by blast
        hence "(\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p) w" using a l by blast}
      hence "(\<^bold>\<box>(q \<^bold>\<leftrightarrow> (\<^bold>\<box>q \<^bold>\<rightarrow> p)) \<^bold>\<rightarrow> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)) w" by blast}
    hence "\<lfloor>\<^bold>\<box>(q \<^bold>\<leftrightarrow> (\<^bold>\<box>q \<^bold>\<rightarrow> p)) \<^bold>\<rightarrow> (\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)\<rfloor>" by blast} 
thus ?thesis by blast
qed

theorem Chap1Theorem24b: "GL \<^bold>\<turnstile> \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<box>A) \<^bold>\<leftrightarrow> \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<top>)" 
proof -
  have 1: "GL \<^bold>\<turnstile>  \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<top>) \<^bold>\<rightarrow> \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<box>A)" by meson
  have 2: "GL \<^bold>\<turnstile> \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<box>A) \<^bold>\<rightarrow> \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<top>)"
  proof - (*Only smt proof found*)
  {assume Lo: GL
    {fix w::i
      have "(\<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<box>A) \<^bold>\<rightarrow> \<^bold>\<box>(A)) w"
        proof -
          {assume "(\<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<box>A)) w"
            from Chap1Theorem19a have "\<lfloor>\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<rightarrow> A) \<^bold>\<leftrightarrow> \<^bold>\<box>A\<rfloor>" using Lo by (rule mp)
            hence "(\<^bold>\<box>A) w" using \<open>w \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> A = v \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> A))))\<close> by blast}
          thus ?thesis by blast qed}
      hence "\<lfloor>\<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<box>A) \<^bold>\<rightarrow> \<^bold>\<box>(A)\<rfloor>" by blast}
    thus ?thesis by blast qed
  thus ?thesis using 1 by blast
qed

theorem Chap1Theorem24c: "GL \<^bold>\<turnstile> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<box>\<^bold>\<not>p) \<^bold>\<leftrightarrow> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<box>\<^bold>\<bottom>)" by metis
theorem Chap1Theorem24d: "GL \<^bold>\<turnstile> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<not>p) \<^bold>\<leftrightarrow> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<bottom>)" 
proof -
  {assume Lo: GL
  {assume red: "\<not> \<lfloor>\<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<not>p) \<^bold>\<leftrightarrow> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<bottom>)\<rfloor>"
   hence "\<exists>q. \<not> \<lfloor>\<^bold>\<box>(q \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<not>q) \<^bold>\<leftrightarrow> \<^bold>\<box>(q \<^bold>\<leftrightarrow> \<^bold>\<bottom>)\<rfloor>" by force
   then obtain q where obtq: "\<not> \<lfloor>\<^bold>\<box>(q \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<not>q) \<^bold>\<leftrightarrow> \<^bold>\<box>(q \<^bold>\<leftrightarrow> \<^bold>\<bottom>)\<rfloor>" by presburger
   have "\<forall>A. (GL \<^bold>\<turnstile> \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<box>A) \<^bold>\<leftrightarrow> \<^bold>\<box>(A \<^bold>\<leftrightarrow> \<^bold>\<top>))" using Chap1Theorem24b by blast
   hence "GL \<^bold>\<turnstile> \<^bold>\<box>((\<^bold>\<not>q) \<^bold>\<leftrightarrow> \<^bold>\<box>(\<^bold>\<not>q)) \<^bold>\<leftrightarrow> \<^bold>\<box>((\<^bold>\<not>q) \<^bold>\<leftrightarrow> \<^bold>\<top>)" by (rule allE)
   hence "\<lfloor>\<^bold>\<box>((\<^bold>\<not>q) \<^bold>\<leftrightarrow> \<^bold>\<box>(\<^bold>\<not>q)) \<^bold>\<leftrightarrow> \<^bold>\<box>((\<^bold>\<not>q) \<^bold>\<leftrightarrow> \<^bold>\<top>)\<rfloor>" using Lo by (rule mp)
   hence "\<lfloor>\<^bold>\<box>(q \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<not>q) \<^bold>\<leftrightarrow> \<^bold>\<box>(q \<^bold>\<leftrightarrow> \<^bold>\<bottom>)\<rfloor>" by fast
  hence False using obtq by blast}
  hence "\<lfloor>\<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<not>p) \<^bold>\<leftrightarrow> \<^bold>\<box>(p \<^bold>\<leftrightarrow> \<^bold>\<bottom>)\<rfloor>" by blast}
  thus ?thesis by blast
qed

(*Two proofs from Chap 4 to include in the paper.*)
theorem Chap4Theorem10a: "(\<And>p. \<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>) \<Longrightarrow> (transitive \<and> converseWellFounded)" 
  proof -
    {assume GLaxiom:"\<And>p.\<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>"
      hence "\<forall>p.\<lfloor>\<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<box> \<^bold>\<box> p\<rfloor>"
      proof - (* This is an example of how proof theoretic notation can easily be used in proofs
                in the embedding and then combined with the "deeper" features of the embedding.*)
      {fix A::\<sigma> (*makes proof easier*)  
        have "\<lfloor>A \<^bold>\<rightarrow> ((\<^bold>\<box>\<^bold>\<box>A \<^bold>\<and> \<^bold>\<box>A) \<^bold>\<rightarrow> (\<^bold>\<box>A \<^bold>\<and>A))\<rfloor>" by force (*Normality*) 
        have "\<lfloor>A \<^bold>\<rightarrow> (\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A) \<^bold>\<rightarrow> (\<^bold>\<box>A \<^bold>\<and>A))\<rfloor>" by meson (*Normality*)  
        have "\<lfloor>\<^bold>\<box>A \<^bold>\<rightarrow> \<^bold>\<box>(\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A) \<^bold>\<rightarrow> (\<^bold>\<box>A \<^bold>\<and>A))\<rfloor>" using GLaxiom by blast
        hence "\<lfloor>\<^bold>\<box>(\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A) \<^bold>\<rightarrow> (\<^bold>\<box>A \<^bold>\<and> A)) \<^bold>\<rightarrow> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A)\<rfloor>" using GLaxiom by (rule allE)  
        hence "\<lfloor>\<^bold>\<box>A \<^bold>\<rightarrow> \<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A)\<rfloor>" by blast   
        hence "\<lfloor>\<^bold>\<box>(\<^bold>\<box>A \<^bold>\<and> A) \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<box>A\<rfloor>" by fast
        hence "\<lfloor>\<^bold>\<box>A \<^bold>\<rightarrow> \<^bold>\<box> \<^bold>\<box> A\<rfloor>" using \<open>\<lfloor>(\<lambda>w. w \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> A)) \<longrightarrow> w \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> A)) \<and> v \<^bold>\<Turnstile> A)))\<rfloor>\<close> by blast}
      then show "\<forall>p.\<lfloor>\<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<box> \<^bold>\<box> p\<rfloor>" by auto qed
      hence tran:"transitive" by blast
      have "converseWellFounded"
      proof -
        {fix S::\<sigma>
        {assume "\<exists>a.(S(a))"
          then obtain a where "S a" by auto  
          {assume "\<not>(\<exists>m.(S(m) \<and> (\<forall>s.(S(s) \<longrightarrow> (\<not>(m r s))))))"
          hence "\<exists>q. \<forall>w.((q w) \<longleftrightarrow> (\<not>S(w)))" by fastforce
          then obtain q where a:"\<forall>w.((q w) \<longleftrightarrow> (\<not>S(w)))" by auto
          have "a \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>q \<^bold>\<rightarrow> q))"
          proof -
            {fix x
            {assume "a r x"
            {assume "x \<^bold>\<Turnstile> q"
            hence "x \<^bold>\<Turnstile>(\<^bold>\<box>q \<^bold>\<rightarrow> q)" by simp}
            hence supfl1: "(x \<^bold>\<Turnstile> q) \<Longrightarrow> (x \<^bold>\<Turnstile>(\<^bold>\<box>q \<^bold>\<rightarrow> q))" by blast (*not needed but helps Isabelle*) 
            {assume "\<not>(x \<^bold>\<Turnstile> q)"
            hence "\<not>(q x)" by presburger  
            hence "\<exists>y. (x r y \<and> S(y))" using \<open>\<nexists>m. m \<^bold>\<Turnstile> S \<and> \<lfloor>(\<lambda>s. s \<^bold>\<Turnstile> S \<longrightarrow> s \<^bold>\<Turnstile> (\<^bold>\<not>(r) m))\<rfloor>\<close> a by blast
            hence "\<not>x \<^bold>\<Turnstile> (\<^bold>\<box> q)" by (simp add: a)
            hence "x \<^bold>\<Turnstile>(\<^bold>\<box>q \<^bold>\<rightarrow> q)" by (simp add: \<open>\<not>x \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> q))\<close>)}
            hence "x \<^bold>\<Turnstile>(\<^bold>\<box>q \<^bold>\<rightarrow> q)" using supfl1 by fast}}
            thus "a \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>q \<^bold>\<rightarrow> q))" by fast qed
          have "\<not>a \<^bold>\<Turnstile> (\<^bold>\<box> q)" using \<open>\<nexists>m. m \<^bold>\<Turnstile> S \<and> \<lfloor>(\<lambda>s. s \<^bold>\<Turnstile> S \<longrightarrow> s \<^bold>\<Turnstile> (\<^bold>\<not>(r) m))\<rfloor>\<close> \<open>a \<^bold>\<Turnstile> S\<close> a by blast
          hence "\<not>a \<^bold>\<Turnstile> (\<^bold>\<box>(\<^bold>\<box>q \<^bold>\<rightarrow> q) \<^bold>\<rightarrow> \<^bold>\<box>q)" using \<open>a \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> q)) \<longrightarrow> v \<^bold>\<Turnstile> q))\<close> by blast
          hence False using GLaxiom by auto}}}
    thus converseWellFounded by presburger qed
    hence "transitive \<and> converseWellFounded" using tran by blast}    
  thus  "(\<And>p. \<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>) \<Longrightarrow> (transitive \<and> converseWellFounded)" by blast
qed

theorem Chap4Theorem10b: "(transitive \<and> converseWellFounded) \<Longrightarrow> \<forall>p. \<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>"
proof -
  {assume asm: "transitive \<and> converseWellFounded"  
  {fix p w
    {assume "\<not>w \<^bold>\<Turnstile> (\<^bold>\<box>p)"
      let ?X = "(\<lambda>x. (w r x \<and> \<not>(x \<^bold>\<Turnstile> p)))"
      have a: "\<exists>a. ?X a" by (metis (no_types) \<open>\<not>w \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> p))\<close>)
      then obtain a where b: "?X a" by auto
      have "\<forall>S::i\<Rightarrow>bool. (\<exists>a.(S(a))) \<longrightarrow> (\<exists>m.(S(m) \<and> (\<forall>s.(S(s) \<longrightarrow> (\<not>(m r s))))))"  using asm by blast  
      hence "(\<exists>b.(?X(b))) \<longrightarrow> (\<exists>m.(?X(m) \<and> (\<forall>s.(?X(s) \<longrightarrow> (\<not>(m r s))))))" by (rule allE)   
      then obtain x where c:"?X(x) \<and> (\<forall>s.(?X(s) \<longrightarrow> (\<not>(x r s))))" using \<open>\<not> w \<^bold>\<Turnstile> (\<^bold>\<box>(\<lambda>v. v \<^bold>\<Turnstile> p))\<close> by auto 
      hence "w \<^bold>\<Turnstile>(\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p)" using  c by (metis asm)}}
  hence "\<forall>p. \<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>" by blast}
  then show "(transitive \<and> converseWellFounded) \<Longrightarrow> \<forall>p. \<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>" by blast
qed
(*>*)

theorem Chap4Theorem10: "(\<forall>p. \<lfloor>\<^bold>\<box>(\<^bold>\<box>p \<^bold>\<rightarrow> p) \<^bold>\<rightarrow> \<^bold>\<box>p\<rfloor>) \<longleftrightarrow> (transitive \<and> converseWellFounded)"
(*<*)
using Chap4Theorem10a Chap4Theorem10b by argo
(*>*)
text {*Besides these elementary theorems, another thing can be proven and automated in GL. 
Isabelle is able to proof Gödel's second Incompleteness Theorem as represented in GL
automatically in a single step.*}
theorem goedel2: "GL \<^bold>\<turnstile> \<^bold>\<not>\<^bold>\<box>\<^bold>\<bottom> \<^bold>\<rightarrow> \<^bold>\<not>\<^bold>\<box>\<^bold>\<not>\<^bold>\<box>\<^bold>\<bottom>" by metis
text {*Of course, this is only half of the work needed to prove the second Incompleteness Theorem formally.
The other half needed is to implement Peano Arithmetic in Isabelle and show that its provability predicate
satisfies the Hilbert-Bernays-Löb derivability conditions and that they are faithfully captured by GL. While this is
a significant task in itself (see the comments by Lawrence Paulson who formalized the Incompleteness
Theorems using a different formalism than Peano Arithmetic \cite{AFP-Incompleteness}), it can be decoupled from the implementation presented here. The embedding
 can live on top of any such project and its proving prowess is not affected by 
any implementation of arithmetic below it.

This paper is part of an ongoing process to systematically verify large swaths of Boolos' \emph{The Logic of Provability}. The source code
for this paper and most theorems of chapter one and four from the book can be found online. \cite{SOURCE}*}

(*<*)

(*CHAPTER 4 OF BOOLOS*)

(*We will introduce two shorthand notations for iterated accessibility relations*)
primrec RpowN :: "nat \<Rightarrow> (i \<Rightarrow> i \<Rightarrow> bool)" ("r\<^sup>_")
  where
    "r\<^sup>0 = (\<lambda>x. \<lambda>y. (x = y))"
    |"r\<^sup>(Suc n) = (\<lambda>w. \<lambda>y. (\<exists>x. ((r\<^sup>n w x) \<and> (x r y))))"

primrec BoxPowN :: "nat \<Rightarrow> \<sigma> \<Rightarrow> \<sigma>" ("\<box>\<^sup>_")
  where
    "(\<box>\<^sup>0 A) = A"
    | "\<box>\<^sup>(Suc n) A = \<^bold>\<box>(\<box>\<^sup>n A)"

primrec DiaPowN :: "nat \<Rightarrow> \<sigma> \<Rightarrow> \<sigma>" ("\<diamond>\<^sup>_")
  where
    "(\<diamond>\<^sup>0 A) = A"
    | "\<diamond>\<^sup>(Suc n) A = \<^bold>\<diamond>(\<diamond>\<^sup>n A)"      

(*Some lemmas to make the following proofs easier.*)
lemma relpowdirection: "\<And>z w y. (z r w \<and> (r\<^sup>i w y)) \<Longrightarrow> (r\<^sup>(Suc i) z y)" 
  proof (induction i)
    case 0
    then show ?case by auto
  next
    case (Suc i)
    obtain x where obt: "((r\<^sup>i w  x) \<and> (x r y))" using Suc.prems by auto  
    from this show ?case using Suc.prems Suc.IH by fastforce
  qed
    
lemma relpowreverse: "\<And>x y.(r\<^sup>(Suc n) x y) \<Longrightarrow> \<exists>z. (x r z \<and> (r\<^sup>n z y))"
proof (induction n)
  case 0
  then show ?case by force
next
  case (Suc n)
  let ?m = "Suc (Suc n)"
  have "(r\<^sup>?m x y)" using Suc.prems by simp
  then obtain w where "(r\<^sup>Suc n x w \<and> (w r y))" by auto  
  then show ?case using Suc.IH Suc.prems by fastforce
qed
  
lemma iteratedDeMorgan: "(\<^bold>\<not>\<diamond>\<^sup>j (\<^bold>\<not>A)) = (\<box>\<^sup>j A)" 
proof (induction j)
  case 0
  then show ?case by simp
next
  case (Suc j)
  hence "\<^bold>\<box> (\<^bold>\<not>\<diamond>\<^sup>j (\<^bold>\<not>A)) = \<^bold>\<box> (\<box>\<^sup>j A)" by metis
  then show ?case by auto
qed

(*Proving the theorems in Chapter 4*)
theorem Chap4Theorem2a: "\<And>w. ((w \<^bold>\<Turnstile> (\<diamond>\<^sup>j A)) \<longleftrightarrow> (\<exists>y.((r\<^sup>j w y) \<and> (y \<^bold>\<Turnstile> A))))" 
proof (induction j)
  case 0
  then show ?case by auto
next
  case (Suc j)    
  then show ?case using relpowdirection relpowreverse Suc.IH by (meson DiaPowN.simps(2))
qed
    
theorem Chap4theorem2b: "(w \<^bold>\<Turnstile> \<box>\<^sup>j A) \<longleftrightarrow> (\<forall>y. ((r\<^sup>j w y) \<longrightarrow> (y \<^bold>\<Turnstile> A)))" 
  by (metis Chap4Theorem2a iteratedDeMorgan)
    
theorem Chap4Theorem5: "(\<forall>p. \<lfloor>\<^bold>\<box>p \<^bold>\<rightarrow> p\<rfloor>) \<longleftrightarrow> reflexive" by auto
    
theorem Chap4Theorem6: "(\<forall>p.\<lfloor>\<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<box> \<^bold>\<box> p\<rfloor>) \<longleftrightarrow> transitive" 
  proof -
    have a: "(\<forall>p.\<lfloor>\<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<box> \<^bold>\<box> p\<rfloor>) \<Longrightarrow> transitive" by auto
    have b: "transitive \<Longrightarrow> (\<forall>p.\<lfloor>\<^bold>\<box>p \<^bold>\<rightarrow> \<^bold>\<box> \<^bold>\<box> p\<rfloor>)" by metis
  thus ?thesis using a b by argo
qed
      
theorem Chap4Theorem7: "(\<forall>p. \<lfloor>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>) \<longleftrightarrow> symmetric" 
proof -
have "symmetric \<longrightarrow> (\<forall>p. \<lfloor>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>)" by blast
have "(\<forall>p. \<lfloor>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>) \<Longrightarrow> symmetric"  
proof -
  {assume asm:"\<forall>p. \<lfloor>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>"
    {fix x y
    {assume sym: "x r y"  
      have "\<exists>q. \<forall>w. (((w \<noteq> x) \<longrightarrow> \<not>(q w)) \<and> (q x))" by fast
      then obtain q where a: "\<forall>w. (((w \<noteq> x) \<longrightarrow> \<not>(q w)) \<and> (q x))" by auto   
      hence "(\<^bold>\<box> \<^bold>\<diamond> q) x" using asm by metis
      hence "(\<^bold>\<diamond>q) y" using sym by simp    
      hence "y r x" using a by auto}}
    hence "symmetric" by blast}    
  thus "(\<forall>p. \<lfloor>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>) \<Longrightarrow> symmetric" by blast qed
thus ?thesis by blast
qed      
      
theorem Chap4Theorem8: "(\<forall>p.\<lfloor>\<^bold>\<diamond>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>) \<longleftrightarrow> euclidean" 
proof -
  have "euclidean \<Longrightarrow> (\<forall>p.\<lfloor>\<^bold>\<diamond>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>)" by blast
  have "(\<forall>p.\<lfloor>\<^bold>\<diamond>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>) \<Longrightarrow> euclidean"
    proof -
  {assume asm: "(\<forall>p.\<lfloor>\<^bold>\<diamond>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>)"
    {fix x y z
    {assume eucl: "x r y \<and> x r z"
      have "\<exists>q. (\<forall>w. ((y r w \<and> w \<noteq> z) \<longrightarrow> (\<not>(q w))) \<and> ((\<^bold>\<diamond>q)x))" using eucl by auto   
      then obtain q where a:"\<forall>w. ((y r w \<and> w \<noteq> z) \<longrightarrow> (\<not>(q w))) \<and> ((\<^bold>\<diamond>q)x)" by auto
      hence "y r z" using asm eucl by (metis (full_types))}}
    hence euclidean by blast}
  thus "(\<forall>p.\<lfloor>\<^bold>\<diamond>p \<^bold>\<rightarrow> \<^bold>\<box>\<^bold>\<diamond>p\<rfloor>) \<Longrightarrow> euclidean" by blast
  qed
  thus ?thesis by blast
qed

(*Helper lemma for future proofs using the equivalence of
finite + transitive + irreflexive = finite + transitive + conv well-founded*)
lemma theorem11helper: "transitive \<Longrightarrow> (\<forall>X. (card X = (Suc i) \<longrightarrow> (\<forall>m.((m \<in> X) \<longrightarrow> (\<exists>s. ((s \<in> X) \<and> (m r s))))) \<longrightarrow> (\<exists>z. z r z)))"
  proof (induction i)
    case 0
    then show ?case using card_eq_SucD by (metis singletonD singletonI)
  next
    case (Suc i)
    {fix X::"i set"
    {assume cardX: "card X = (Suc (Suc i))"
    and cd: "(\<forall>m.((m \<in> X) \<longrightarrow> (\<exists>s. ((s \<in> X) \<and> (m r s)))))"
        then obtain d where obtd: "d \<in> X" using card_eq_SucD by blast
        let ?smallX = "X - {d}"
        have cardSmall: "card ?smallX = Suc i" using cardX obtd by (metis card_Suc_Diff1 card_Suc_eq card_empty card_infinite insert_not_empty old.nat.inject)
        {assume a: "(\<forall>m.((m \<in> ?smallX) \<longrightarrow> (\<exists>s. ((s \<in> ?smallX) \<and> (m r s)))))"
          hence "\<exists>z. z r z" using cardSmall cd Suc.IH Suc.prems by blast}
        {assume b: "\<not>(\<forall>m.((m \<in> ?smallX) \<longrightarrow> (\<exists>s. ((s \<in> ?smallX) \<and> (m r s)))))"
          then obtain m where obtm: "m \<in> ?smallX \<and> (\<forall>s \<in> ?smallX. (\<not>(m r s)))" by auto
          then obtain z where obtz: "z \<in> X \<and> d r z" using obtd cd by auto
          hence mrz: "m r z" using Suc.prems by (metis DiffD1 cd insertE insert_Diff obtd obtm)
          hence "\<exists>z. z r z" using obtz DiffD1 cd insertE insert_Diff obtd obtm by blast}
          hence "(\<forall>m.((m \<in> X) \<longrightarrow> (\<exists>s. ((s \<in> X) \<and> (m r s))))) \<longrightarrow> (\<exists>z. z r z)"
            using \<open>\<lfloor>(\<lambda>m. m \<in> X - {d} \<longrightarrow> (\<exists>s. s \<in> X - {d} \<and> s \<^bold>\<Turnstile> (r) m))\<rfloor> \<Longrightarrow> \<exists>z. z \<^bold>\<Turnstile> (r) z\<close> by blast }}
        then show ?case by blast
  qed

lemma theorem11helper2: "transitive \<longrightarrow> (\<forall>X. (card X  > 0 \<longrightarrow> (\<forall>m.((m \<in> X) \<longrightarrow> (\<exists>s. ((s \<in> X) \<and> (m r s))))) \<longrightarrow> (\<exists>z. z r z)))" using theorem11helper by (metis Suc_pred)

abbreviation finiteWorlds :: "bool"
  where "finiteWorlds \<equiv> finite (UNIV :: i set)"

theorem Chap4Theorem11: "(finiteWorlds \<and> transitive) \<Longrightarrow> (irreflexive \<longleftrightarrow> converseWellFounded)"
proof -(*Provers (leo2 and vampire esp.) come up with weird proofs.*)
  {assume H: "finiteWorlds \<and> transitive"
    have l: "converseWellFounded \<longrightarrow> irreflexive" by fast
  {assume irr: "irreflexive"
      {assume "\<not> converseWellFounded"
        then obtain S a where aobt: "(a \<in> S) \<and> \<not>(\<exists>m \<in> S. (\<forall>s \<in> S. (\<not>(m r s))))" by (metis mem_Collect_eq)
        have cardS: "card S > 0" using aobt H by (metis card_eq_0_iff empty_iff finite_subset gr0I top.extremum)
        hence False using irr cardS aobt H theorem11helper2 by blast}
    hence converseWellFounded by blast}
  hence "irreflexive \<longleftrightarrow> converseWellFounded" using l by auto}
  thus "(finiteWorlds \<and> transitive) \<Longrightarrow> (irreflexive \<longleftrightarrow> converseWellFounded)" by blast
qed
end
(*>*)
